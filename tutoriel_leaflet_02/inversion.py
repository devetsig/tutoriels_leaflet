import json as j #chercher le module JSON pour ouvrir notre fichier geoJSON

with open('data/contours.geojson') as contours:
    data = j.load(contours) #on charge le JSON dans une variable pour l'exploiter dans la boucle

#là on va dérouler le geoJSON
for feature in data['features']:
    coordinates = feature['geometry']['coordinates']
    for coordinate in coordinates:
        for lnglat in coordinate:
            lnglat.reverse() #la méthode reverse va inverser l'ordre de nos objets à savoir les latitudes avec les longitudes
            print(lnglat)
